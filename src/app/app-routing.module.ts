import { QuestionOneComponent } from './question-one/question-one.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuestionTwoComponent } from './question-two/question-two.component';

const routes: Routes = [
  {path: 'question-1', component: QuestionOneComponent},
  {path: 'question-2', component: QuestionTwoComponent},
  { path: '',   redirectTo: 'question-1', pathMatch: 'full' },
]
;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

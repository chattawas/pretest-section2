import { Component, OnInit } from '@angular/core';
import { ApiService } from '../_service/api.service';

@Component({
  selector: 'app-question-two',
  templateUrl: './question-two.component.html',
  styleUrls: ['./question-two.component.css']
})
export class QuestionTwoComponent implements OnInit {
  categories: any[];
  filter = '';
  filterData: any[];
  constructor(private apiService: ApiService) {
    this.apiService.getData().subscribe(data => {
      this.categories = data;
      this.filterData = this.categories;
    });
  }

  ngOnInit() {
  }

  filterChange(){
    this.filterData = this.categories.filter(x => x.includes(this.filter));
  }

}

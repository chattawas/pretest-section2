import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-question-one',
  templateUrl: './question-one.component.html',
  styleUrls: ['./question-one.component.css']
})
export class QuestionOneComponent implements OnInit {

  integerValue = 0; //
  calculation = ['isPrime', 'isFibonacci'];
  calculationValue = 'isPrime';
  constructor() { }

  ngOnInit() {
  }

  calculateSummary(){
    if (this.integerValue && this.integerValue != 0) {
      if (this.calculationValue == 'isPrime'){
        return this.prime(this.integerValue);
      }else {
        return this.isFibonacci(this.integerValue);
      }
    }
  }

  prime(n){
    if (n < 2) return false;
    var q = Math.floor(Math.sqrt(n));
    for (var i = 2; i <= q; i++)
    {
        if (n % i == 0)
        {
            return false;
        }
    }

    return true;
  }

  isFibonacci(n) {
    let fib = 0;
    const a = (5 * Math.pow(n, 2) + 4);
    const b = (5 * Math.pow(n, 2) - 4);

    const result = Math.sqrt(a) % 1 == 0;
    const res = Math.sqrt(b) % 1 == 0;

    //fixed this line
    if (result || res == true) // checks the given input is fibonacci series
    {
      fib = Math.round(n * 1.618); // finds the next fibonacci series of given input
      console.log('The next Fibonacci number is ' + fib);
      return true;
    } else {
      console.log(`The given number ${n} is not a fibonacci number`);
      return false;
    }
  }
}
